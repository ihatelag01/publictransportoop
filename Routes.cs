﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransport_OOP
{
    class Routes
    {
        public List<Stations> lst = new List<Stations>();

        public void addToLst(Stations st)
        {
            lst.Add(st);
        }

        public void showRoute()
        {
            Console.WriteLine("Route:");
            foreach (var VARIABLE in lst)
            {
                Console.Write(VARIABLE.getId()+"->");
            }
        }

    }
}
