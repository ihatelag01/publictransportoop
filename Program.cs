﻿using System;

namespace PublicTransport_OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            Passenger pass = new Passenger();
            pass.name = "Ionut";
            pass.message();
            Driver dv = new Driver();
            dv.name = "Daniel";
            dv.message();
            pass.message("Have a nice day!");
            Stations st1 = new Stations();
            st1.setId(1);
            Stations st2 = new Stations();
            st2.setId(2);
            Stations st3 = new Stations();
            st3.setId(3);
            Routes rt1 = new Routes();
            rt1.addToLst(st1);
            rt1.addToLst(st2);
            ;
            Bus b1 = new Bus();
            b1.showType();
            Tram t1 = new Tram();
            t1.showType();
            rt1.showRoute();
        }
    }
}
