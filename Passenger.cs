﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublicTransport_OOP
{
    class Passenger:People, IHello
    {
        public void message()
        {
            Console.WriteLine("Hello,my name is "+this.name+" and I am a Passanger");
        }

        public void message(string str)
        {
            Console.WriteLine(this.name+" says "+str);
        }
       
    }
}
